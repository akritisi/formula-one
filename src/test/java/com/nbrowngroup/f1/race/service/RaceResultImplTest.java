package com.nbrowngroup.f1.race.service;

import com.nbrowngroup.f1.race.beans.RaceTopResult;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class RaceResultImplTest {
    @InjectMocks
    private  RaceResultImpl raceResultImpl;

    @Before
    public void setUp() {

        raceResultImpl = new RaceResultImpl();
    }

    @Test
    public void shouldReturnTopRaceResult_whenGetResultsCalled() {

        List<RaceTopResult> raceTopResultList = raceResultImpl.getResults();

        assertFalse(raceTopResultList.isEmpty());
        assertEquals(10, raceTopResultList.size());
    }

}