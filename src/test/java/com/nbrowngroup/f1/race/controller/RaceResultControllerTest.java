package com.nbrowngroup.f1.race.controller;

import com.nbrowngroup.f1.race.beans.RaceTopResult;
import com.nbrowngroup.f1.race.config.AppConfig;
import com.nbrowngroup.f1.race.service.RaceResultImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import java.util.Arrays;
import java.util.List;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebAppConfiguration
public class RaceResultControllerTest {

    @Mock
    private RaceResultImpl raceResult;

    @InjectMocks
    private RaceResultController controller;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void shouldReturnMainPage_whenGetRequestForMainPage() throws Exception {

        List<RaceTopResult> raceTopResultList = getRaceTopResults();
        when(raceResult.getResults()).thenReturn(raceTopResultList);

        mockMvc.perform(get("/main"))
               .andExpect(status().isOk())
               .andExpect(view().name("race"));

    }

    private List<RaceTopResult> getRaceTopResults() {
        RaceTopResult raceTopResult = new RaceTopResult();
        raceTopResult.setTeam("team");
        raceTopResult.setDriver("driver");
        return Arrays.asList(raceTopResult);
    }

}