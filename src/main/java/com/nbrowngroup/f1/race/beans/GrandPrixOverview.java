package com.nbrowngroup.f1.race.beans;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class GrandPrixOverview {

    private Race race = new Race();
    private String raceDate;
    private Driver driver =  new Driver();
    private Team team = new Team();
    private String laps;
    private String timeTaken;
}