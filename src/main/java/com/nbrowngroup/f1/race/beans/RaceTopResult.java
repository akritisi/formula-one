package com.nbrowngroup.f1.race.beans;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RaceTopResult {

    private String driver;
    private String team;
    private Integer driverTotalPoints;
    private Integer teamTotalPoints;

}

