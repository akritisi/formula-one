package com.nbrowngroup.f1.race.beans;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Race {

    private String title;
    private String url;
}