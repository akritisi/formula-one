package com.nbrowngroup.f1.race.beans;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Team {

    private String teamName;
    private String teamUrl;
}