package com.nbrowngroup.f1.race.beans;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Driver {

    private String driverName;
    private String driverUrl;
}