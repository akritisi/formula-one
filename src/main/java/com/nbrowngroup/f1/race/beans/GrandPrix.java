package com.nbrowngroup.f1.race.beans;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GrandPrix {

    private Driver driver = new Driver();
    private Team team = new Team();
    private String grid;
    private Integer points;
    private String laps;
    private String timeTaken;
}