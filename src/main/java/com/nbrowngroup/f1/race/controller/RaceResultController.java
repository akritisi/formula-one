package com.nbrowngroup.f1.race.controller;

import com.nbrowngroup.f1.race.beans.RaceTopResult;
import com.nbrowngroup.f1.race.service.RaceResultImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@Controller
public class RaceResultController {

    @Autowired
    RaceResultImpl raceResultImpl;

    @RequestMapping(value="/main", method = RequestMethod.GET)
    public String getMainPage() {
        return "race";
    }

    @RequestMapping(value="/results", method = RequestMethod.GET)
    @ResponseBody
    public List<RaceTopResult> getResults() throws Exception {

        List<RaceTopResult> raceTopResultList = raceResultImpl.getResults();
        return raceTopResultList;
    }
}
