package com.nbrowngroup.f1.race.service;

import com.nbrowngroup.f1.race.beans.GrandPrix;
import com.nbrowngroup.f1.race.beans.GrandPrixOverview;
import com.nbrowngroup.f1.race.beans.RaceTopResult;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class RaceResultImpl implements RaceResult {

    private static final Logger LOGGER = Logger.getLogger(RaceResultImpl.class.getName());

    private static final String DRIVER_CLASS_NAME = "msr_col3";
    private static final String TEAM_CLASS_NAME = "msr_col4";
    private static final Integer DRIVER_TOP_COUNT = 10;
    private static final Integer TEAM_TOP_COUNT = 5;
    private static final String url = "https://www.f1-fansite.com/f1-results/2018-f1-championship-standings/";

    @Override
    public List<RaceTopResult> getResults() {
        List<RaceTopResult> raceTopResultList = new ArrayList<>();
        try {

            Document mainPageDocument =  Jsoup.connect(url).get();

            List<Element> trElementList = getGrandPrixOverviewTableElements(mainPageDocument);
            List<GrandPrixOverview> grandPrixOverviewList = elementsToGrandPrixList(trElementList);

            Document innerPageDocument = Jsoup.connect(grandPrixOverviewList.get(0).getRace().getUrl()).get();
            List<String> uniqueDrivers = getUniqueList(innerPageDocument, DRIVER_CLASS_NAME);
            List<String> uniqueTeams = getUniqueList(innerPageDocument, TEAM_CLASS_NAME);

            List<GrandPrix> grandPrixList = getGrandPrixList(grandPrixOverviewList);

            Map<String, Integer> uniqueDriverTotalPointsMap = new HashMap<>();
            Map<String, Integer> uniqueTeamsTotalPointsMap =  new HashMap<>();


            uniqueDriverTotalPointsMap.putAll(getTotalPointsForDriversMap(uniqueDrivers, grandPrixList));
            uniqueTeamsTotalPointsMap.putAll(getTotalPointsForTeamsMap(uniqueTeams, grandPrixList));

            Map<String, Integer> uniqueSortedTopDriversMap = getUniqueSortedTopMap(uniqueDriverTotalPointsMap, DRIVER_TOP_COUNT);
            Map<String, Integer> uniqueSortedTopTeamsMap = getUniqueSortedTopMap(uniqueTeamsTotalPointsMap, TEAM_TOP_COUNT);

             raceTopResultList = getRaceTopResultList(uniqueSortedTopDriversMap,uniqueSortedTopTeamsMap);

        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }

        return raceTopResultList;
    }

    private List<Element> getGrandPrixOverviewTableElements(Document document) {

        Element tBodyElement = document.select("table[class=motor-sport-results msr_season_summary tablesorter]")
                                       .first().getElementsByTag("tbody").first();
        return tBodyElement.getElementsByTag("tr");
    }

    private List<GrandPrixOverview> elementsToGrandPrixList(List<Element> grandPrixElements) {

        List<GrandPrixOverview> grandPrixOverviewList = new ArrayList<>();
        grandPrixElements.stream().parallel().forEach(grandPrixTr -> {
            final GrandPrixOverview grandPrixOverviewRow =  new GrandPrixOverview();
            grandPrixTr.getElementsByTag("td").stream().forEach(grandPrixTd ->
                elementToGranPrixConverter(grandPrixTd, grandPrixOverviewRow));
            grandPrixOverviewList.add(grandPrixOverviewRow);                     }
    );
        return grandPrixOverviewList;
    }

    private void elementToGranPrixConverter(Element grandPrixTd, GrandPrixOverview grandPrixOverviewRow) {

        String grandPrixTdClassName = grandPrixTd.className();

        switch(grandPrixTdClassName) {
            case "msr_col1":
                grandPrixOverviewRow.getRace().setUrl(grandPrixTd.getElementsByTag("a").select("a[href]")
                                                                 .attr("abs:href"));
                grandPrixOverviewRow.getRace().setTitle(grandPrixTd.children().first().text());
                break;
            case "msr_col2":
                grandPrixOverviewRow.setRaceDate(grandPrixTd.text());
                break;
            case "msr_col3":
                grandPrixOverviewRow.getDriver().setDriverUrl(grandPrixTd.getElementsByTag("a").select("a[href]")
                                                                         .attr("abs:href"));
                grandPrixOverviewRow.getDriver().setDriverName(grandPrixTd.getElementsByTag("a").first().text());
                break;
            case "msr_col4":
                grandPrixOverviewRow.getTeam().setTeamUrl(grandPrixTd.getElementsByTag("a").select("a[href]")
                                                                     .attr("abs:href"));
                grandPrixOverviewRow.getTeam().setTeamName(grandPrixTd.getElementsByTag("a").first().text());
                break;
            case "msr_col5":
                grandPrixOverviewRow.setLaps(grandPrixTd.text());
                break;
            case "msr_col6":
                grandPrixOverviewRow.setTimeTaken(grandPrixTd.text());
                break;
        }

    }

    private List<String> getUniqueList(Document innerPageDocument, String className) {
        List<Element> trElements = innerPageDocument.getElementById("msr_result")
                                                    .getElementsByTag("tbody").first()
                                                    .getElementsByTag("tr");
        List<Element> tdElements = new ArrayList<>();

        trElements.stream().forEach(tr -> {
            tr.getElementsByTag("td").stream()
              .filter(td -> td.className().equals(className))
              .forEach(filteredTd -> tdElements.add(filteredTd));
        });

        List<String> data = new ArrayList<>();

        tdElements.stream().distinct().forEach(tdElement -> data.add(tdElement.getElementsByTag("a").first().text()));
        return data;
    }

    private List<GrandPrix> getGrandPrixList(List<GrandPrixOverview> grandPrixOverviewList) {

        List<GrandPrix> grandPrixList = new ArrayList<>();
        grandPrixOverviewList.stream().parallel().forEach(grandPrixOverview -> {
            try {
                Document innerPageDocumentForResults = Jsoup.connect(grandPrixOverview.getRace().getUrl()).get();
                grandPrixList.addAll(getListForInnerPage(innerPageDocumentForResults));

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        return grandPrixList;
    }

    private List<GrandPrix> getListForInnerPage(Document innerPageDocument) {
        List<Element> trElements = innerPageDocument.getElementById("msr_result")
                                                    .getElementsByTag("tbody").first()
                                                    .getElementsByTag("tr");

        List<GrandPrix> grandPrixList = new ArrayList<>();

        trElements.forEach( trElement -> { GrandPrix grandPrix = new GrandPrix();
            trElement.getElementsByTag("td").forEach(tdElement -> {
                String tdClassName = tdElement.className();
                switch(tdClassName) {
                    case "msr_col3":
                        grandPrix.getDriver().setDriverName(tdElement.getElementsByTag("a").first().text());
                        break;
                    case "msr_col4":
                        grandPrix.getTeam().setTeamName(tdElement.getElementsByTag("a").first().text());
                        break;
                    case "msr_col5":
                        grandPrix.setTimeTaken(tdElement.text());
                        break;
                    case "msr_col6":
                        grandPrix.setLaps(tdElement.text());
                        break;
                    case "msr_col8":
                        grandPrix.setGrid(tdElement.text());
                        break;
                    case "msr_col9":
                        grandPrix.setPoints(Integer.parseInt(tdElement.text()));
                        break;
                }
            });
        grandPrixList.add(grandPrix);
        });
        return grandPrixList;
    }

    private Map<String, Integer> getTotalPointsForDriversMap(List<String> uniqueList, List<GrandPrix> grandPrixList) {

        Map<String, Integer> map = new HashMap<>();

        uniqueList.forEach(driver -> {
            AtomicReference<Integer> driverTotalPoints = new AtomicReference<>(0);
            grandPrixList.stream()
                         .filter(grandPrix -> grandPrix.getDriver().getDriverName().equals(driver))
                         .forEach(grandPrix -> {
                             driverTotalPoints.set(driverTotalPoints.get() + grandPrix.getPoints());
                         });
            map.put(driver, driverTotalPoints.get());
        });

        return map;
    }

    private Map<String, Integer> getTotalPointsForTeamsMap(List<String> uniqueList, List<GrandPrix> grandPrixList) {

        Map<String, Integer> map = new HashMap<>();

        uniqueList.forEach(team -> {
            AtomicReference<Integer> teamTotalPoints = new AtomicReference<>(0);
            grandPrixList.stream()
                         .filter(grandPrix -> grandPrix.getTeam().getTeamName().equals(team))
                         .forEach(grandPrix -> {
                             teamTotalPoints.set(teamTotalPoints.get() + grandPrix.getPoints());
                         });
            map.put(team, teamTotalPoints.get());
        });

        return map;
    }

    private Map<String, Integer>  getUniqueSortedTopMap(Map<String, Integer> totalPointsMap, Integer count) {

        Map<String, Integer> uniqueDriverTotalPointsMapSortedByMap = totalPointsMap.entrySet().stream()
                                                                                   .sorted((Map.Entry.<String, Integer>comparingByValue().reversed()))
                                                                                   .collect(Collectors.toMap(
                                                                                   Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        Map<String, Integer> topCountMap =    new LinkedHashMap<>();
        AtomicReference<Integer> index = new AtomicReference<>(0);
        uniqueDriverTotalPointsMapSortedByMap.forEach((k,v) -> { index.getAndSet(index.get() + 1);
                                                                        if(index.get() <= count) {
                                                                            topCountMap.put(k,v);
                                                                        }
        });
        return topCountMap;
    }

    private List<RaceTopResult> getRaceTopResultList(Map<String, Integer> uniqueSortedTopDriversMap, Map<String, Integer> uniqueSortedTopTeamsMap) {

        List<RaceTopResult> raceTopResultList = new LinkedList<>();
        Integer index = 0;
        Iterator it = uniqueSortedTopDriversMap.entrySet().iterator();

        while (it.hasNext()) {

            RaceTopResult raceTopResult = new RaceTopResult();
            Map.Entry pair = (Map.Entry)it.next();
            raceTopResult.setDriver((String) pair.getKey());
            raceTopResult.setDriverTotalPoints((Integer) pair.getValue());
            if(index < TEAM_TOP_COUNT ) {
                raceTopResult.setTeam(uniqueSortedTopTeamsMap.entrySet()
                                                             .stream()
                                                             .sorted(Comparator.comparing(Map.Entry::getValue))
                                                             .map(Map.Entry::getKey)
                                                             .collect(Collectors.toList()).get(TEAM_TOP_COUNT - (index+1)));
                raceTopResult.setTeamTotalPoints(uniqueSortedTopTeamsMap.entrySet()
                                                             .stream()
                                                             .sorted(Comparator.comparing(Map.Entry::getValue))
                                                             .map(Map.Entry::getValue)
                                                             .collect(Collectors.toList()).get(TEAM_TOP_COUNT - (index+1)));
            }
            index++;
            raceTopResultList.add(raceTopResult);
        }

        return raceTopResultList;
    }

}