package com.nbrowngroup.f1.race.service;

import com.nbrowngroup.f1.race.beans.RaceTopResult;

import java.io.IOException;
import java.util.List;

public interface RaceResult {

    public List<RaceTopResult> getResults() throws IOException;
}