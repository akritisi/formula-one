$( document ).ready(function() {

	$('button').click(function() {

		document.getElementById("my_table").innerHTML = "<div id=\"loader\"" +
			"                    <p>Loading please wait...</p>\n" +
			"                    <div id=\"progress\" class=\"line stripesLoader\" style=\"background-position:5%;background-color:#17a2b8\"></div>\n" +
			"                </div>"

		changeProgress();
		getData();
	});

	function changeProgress(){

		var progress=document.getElementById("progress");
		for (i=10; i<100; i++) {
			progress.style.backgroundPosition=i+"%";
		}
	}

	function getData() {

		var before = Date.now();
		$.ajax({
			url: "/formula1/results",
			type: "GET",
			dataType: "json"
		})
			.then(
				function success(data) {
						var tableHeader = "<thead class=\"thead-dark\">\n" +
							"    <tr>\n" +
							"      <th scope=\"col\">Rank</th>\n" +
							"      <th scope=\"col\">Team</th>\n" +
							"      <th scope=\"col\">Team's Total Points</th>\n" +
							"      <th scope=\"col\">Driver</th>\n" +
							"      <th scope=\"col\">Driver's Total Points</th>\n" +
							"    </tr>\n" +
							"  </thead>";
						var tableContent = "";

						for (i = 0; i < data.length; i++) {
							var rank = i+1;
							var driver = data[i].driver;
							var team = data[i].team;
							var driverTotalPoints = data[i].driverTotalPoints;
							var teamTotalPoints = data[i].teamTotalPoints;

							if (team == null) {
								team = "-";
							}

							if (teamTotalPoints == null) {
								teamTotalPoints = "-";
							}
							tableContent = tableContent + "<tr><td>" +  rank + "</td><td>" + team + "</td><td>" + teamTotalPoints
								+ "</td><td>" + driver + "</td><td>" + driverTotalPoints + "</td></tr>";
						}
							document.getElementById("loader").remove();
							document.getElementById("my_table").innerHTML = tableHeader + tableContent;

					var after = Date.now();
					console.log('Time taken to get data: ' + (after - before));
				},
					function fail(data, status) {
						console.log('Request failed with return status of ' + status);
					}
				);
	}
});