# FORMULA ONE RESULTS
The application will extract grand prix information from the site below and output the drivers and teams results. The driver’s results will show the top 10 drivers and the team results will show the top 5 teams. Both results will show and be ordered based on the total points scored.

The following link provides a list of grand prix within the season with links to each race result. 

https://www.f1-fansite.com/f1-results/2018-f1-championship-standings/


###1. Technologies
* Spring 4.3.0.RELEASE
* Java 8
* Tomcat 7
* JQuery
* JSP
* Junit
* Mockito

###2. To Run this project locally
```shell
$ git clone https://akritisi@bitbucket.org/akritisi/formula-one.git
$ cd formula-one
$ mvn tomcat7:run
```
Access ```http://localhost:8080/formula1/main```
